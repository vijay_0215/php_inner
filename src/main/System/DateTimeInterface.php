<?php
declare(strict_types=1);

namespace Dreamcat\PhpInner\System;

/**
 * 日期和时间相关的函数
 * @author vijay
 */
interface DateTimeInterface
{
    /**
     * 时间格式化
     * @param string $format 格式化字符串
     * @param int $timestamp 时间戳，不传默认使用当前时间
     * @return string
     */
    public function date(string $format, int $timestamp = null): string;

    /**
     * 获取当前时间戳
     * @return int 当前时间戳
     */
    public function time(): int;

    /**
     * 将时间字符串转为时间戳
     * @param string $time 时间字符串
     * @param int $now 相对时间
     * @return int
     */
    public function strtotime(string $time, int $now = null): int;
}

# end of file
