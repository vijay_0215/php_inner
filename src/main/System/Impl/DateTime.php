<?php
declare(strict_types=1);

namespace Dreamcat\PhpInner\System\Impl;

use Dreamcat\PhpInner\System\DateTimeInterface;

/**
 * 日期和时间相关的函数
 * @author vijay
 */
class DateTime implements DateTimeInterface
{
    /** @inheritDoc */
    public function date(string $format, int $timestamp = null): string
    {
        if ($timestamp === null) {
            $timestamp = $this->time();
        }
        return date($format, $timestamp);
    }

    /** @inheritDoc */
    public function time(): int
    {
        return time();
    }

    /**
     * @inheritDoc
     */
    public function strtotime(string $time, int $now = null): int
    {
        if ($now === null) {
            $now = $this->time();
        }
        return strtotime($time, $now);
    }
}

# end of file
